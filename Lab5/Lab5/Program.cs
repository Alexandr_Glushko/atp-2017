﻿using System;

namespace Lab5
{
	class MainClass
	{
		public static void Main(string[] args)
		{
            double[,] mas = new double[4, 4];
            Random ran = new Random();
            double max = -1;
            double min = 101;
            int[] maxID = new int[2];
            int[] minID = new int[2];
            int[] mx = new int[2];
            int[] mn = new int[2];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    mas[i, j] = ran.Next(0, 100);
                    if (mas[i, j] < min) {
                        min = mas[i, j];
                        minID[0] = i;
                        minID[1] = j;
                    }
                    if (mas[i, j] > max) {
                        max = mas[i, j];
                        maxID[0] = i;
                        maxID[1] = j;
                    }

                    Console.Write(mas[i, j]+" ");
                }
                Console.WriteLine();
            }
            mas[maxID[0], maxID[1]] = 1;
            mas[minID[0], minID[1]] = 1;
            mx[0] = maxID[0];
            mx[1] = maxID[1];
            mn[0] = minID[0];
            mn[1] = minID[1];
            maxID[0] = maxID[0] + 1;
            maxID[1] = maxID[1] + 1;
            minID[0] = minID[0] + 1;
            minID[1] = minID[1] + 1;
            Console.WriteLine("max= " + max + " (" + maxID[0] + "," + maxID[1] + "); min= " + min + " (" + minID[0] + "," + minID[1] + ")");
            for (int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 4; j++)
                {
                        Console.Write((mas[i, j]) + " ");                 
                }
                Console.WriteLine();
            }
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if ((i == mx[0] && j == mx[1]) || (i == mn[0] && j == mn[1]))
                    {
                        Console.Write(1 + " ");
                    }
                    else
                    {
                        Console.Write((mas[i, j] / max) + " ");
                    }
                }
                Console.WriteLine();
            }
            Console.ReadKey();

		}
	}
}
