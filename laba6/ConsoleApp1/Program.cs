﻿using System;
using System.Linq;

namespace Lab6
{
    public struct tt
    {
        public string type, punkt;
        public int places, bagaz, nomer;
        public double time;

        public tt(string a1, int a2, int a3, int a4, string a5, double a6)
        {
            type = a1;
            places = a2;
            bagaz = a3;
            nomer = a4;
            punkt = a5;
            time = a6;
        }
    };

    class MainClass
    {
        static void Main()
        {
            tt[] bus = new tt[5];
            bus[0] = new tt("PAZ", 30, 100, 13, "Kremen", 12.30);
            bus[1] = new tt("Gazzzel`", 22, 80, 96, "Kyiv", 4.20);
            bus[2] = new tt("Mersedes Benz", 16, 60, 22, "Dnipro", 16.45);

            double maxPageCount = 0;
            int maxIndex = 0;

            for (int i = 0; i < 3; i++)
            {
                if (bus[i].places > maxPageCount)
                {
                    maxPageCount = bus[i].places;
                    maxIndex = i;
                }
            }

            Console.WriteLine("The bus with the highest count of places is {0}, Its is {1}.", bus[maxIndex].type, bus[maxIndex].places);
            Console.ReadKey();
        }
    }
}