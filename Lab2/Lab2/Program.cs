﻿using System;

namespace Lab2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("enter X:");
            try
            {
                double x = Convert.ToDouble(Console.ReadLine());
                x = Math.Asin(x + 2) / 3 * (x - 4) + (x * x + 1);
                Console.WriteLine(x);
            }
            catch (Exception EX)
            {
                Console.WriteLine("Вводить можно только цыферки");
            }
            Console.ReadKey();
        }
    }
}
