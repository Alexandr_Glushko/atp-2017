﻿using System;

namespace Lab4
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			double i = 0.5;
			double sum = 0;
			while (i  < 4.8)
			{
				double x = (i + 2 * Math.Pow(i, 3)) - Math.Sqrt(i);
				Console.WriteLine(x);
				sum = sum + x;
				i = i + 0.2;
			}
			Console.WriteLine("sum = " + sum);
			Console.ReadKey();
		}
	}
}
