﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication95
{
    class Program
    {
        public static double A(double x)
        {
            return Math.Sin(3*x) + Math.Pow(x,4);
        }

        public static double B(double x)
        {
            return Math.Sqrt(x) - Math.Log(x);
        }

        public static double C(double x)
        {
            return (4 * x)- 5*Math.Pow(x,3);
        }

        public static double H(double a, double b, double c)
        {
            return Math.Pow(a, 3) + Math.Pow(b, 2) - 8 * c;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Вывод: ");
            try
            {
                double x = Convert.ToDouble(Console.ReadLine());
                double a = A(x);
                double b = B(x);
                double c = C(x);
                double h = H(a, b, c);
                Console.WriteLine("H() = {0}", h);
            }
            catch (Exception EX)
            {
                Console.WriteLine("Вводить можно только цыферки");
            }
            Console.ReadLine();

        }
    }
}
